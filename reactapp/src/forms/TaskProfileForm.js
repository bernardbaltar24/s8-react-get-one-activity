import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const MemberProfileForm = ({task}) => {

	const {description, memberId} = task

  return (
    <Form>
      <FormGroup>
      <FormGroup>
        <Label for="description" className="mt-3">Description</Label>
        <Input type="text" name="description" id="description" placeholder="with a placeholder" value={description} />
      </FormGroup>
        <Label for="firstname" className="mt-3">First Name</Label>
        <Input type="text" name="firstname" id="firstname" placeholder="with a placeholder" value = {memberId ? memberId.firstName: "N/A"} />
      </FormGroup>
      <FormGroup>
        <Label for="lastname" className="mt-3">Last Name</Label>
        <Input type="text" name="lastname" id="lastname" placeholder="with a placeholder" value = {memberId ? memberId.lastName: "N/A"} />
      </FormGroup>

      <div className="text-center"><Button className="mb-3 primary">Save Changes</Button></div>
    </Form>
  );
}

export default MemberProfileForm;