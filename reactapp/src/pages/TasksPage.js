import React, {useState, useEffect} from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import TasksForm from '../forms/TasksForm';
import TasksTableHead from '../tables/TasksTableHead';
import axios from 'axios';


const TasksPage = (props) => {

  console.log("TasksPage props", props.token)

  const[tasksData, setTasksData] = useState({
      token: props.token,
      tasks: []
    })

    const {token, tasks} = tasksData;

    console.log("TASKSSPAGE22 TOKEN", token)

    const getTasks = async (query="") => {
      try{
        const config = {
        headers : {
          Authorization: `Bearer ${token}`
        }
      }

        const res = await axios.get(`http://localhost:5000/tasks${query}`, config)
        console.log("TASKS PAGE res", res)

        return setTasksData({
          ...tasksData,
          tasks: res.data
        })
      }catch(e){
        console.log("TASKS ERROR", e)
      }
    }


    //-------------------COMPLETING TASK--------------

    const completeTask = async(id) => {
      try{
        const config = {
        headers : {
          Authorization: `Bearer ${token}`
          }
        }

        const res = await axios.delete(`http://localhost:5000/tasks/${id}`, config)

        getTasks()

      } catch(e){
        console.log("ERROR SA DELETE MEMBER", e)
      }
    }
  
  useEffect(() => {
    getTasks()
  },[setTasksData])

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Tasks Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<TasksForm/>
        </Col>
        <Col md="8" className="">
        <div>
            <Button className="btn-sm border mr-1" onClick={()=>getTasks()}>
              Get All
            </Button>
            <Button className="btn-sm border mr-1" onClick={()=>getTasks("?isCompleted=false")}>
              Get Ongoing Task
            </Button>
            <Button className="btn-sm border mr-1" className="btn-sm border mr-1" onClick={()=>getTasks("?isCompleted=true")}>
              Get Completed Task
            </Button>
          </div>
          <hr/>
        	<TasksTableHead tasksAttr={tasks} completeTask={completeTask} />
        </Col>
      </Row>
    </Container>
  );
}

export default TasksPage;