import React, {useState, useEffect} from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import TeamsForm from '../forms/TeamsForm';
import TeamsTableHead from '../tables/TeamsTableHead';
import axios from 'axios';

const TeamsPage = (props) => {
  const [teamsData, setTeamsData] = useState({
    token: props.token,
    teams: []
  })
  const {token, teams} = teamsData;

  const getTeams = async (query="") => {
    try {
      const config = {
        headers : {
          Authorization: `Bearer ${token}`
        }
      }

      const res = await axios.get(`http://localhost:5000/teams${query}`, config)
      console.log("Team res", res)

      return setTeamsData({ //if walang useFFect mag rarun sya ng infinite loop
        ...teamsData,
        teams: res.data
      })

    }catch(e){
      console.log("Teams error", e)
      //Swal
    }
  }

  //---------------SOFT DELETE TEAMS------------

  const deleteTeam = async(id)=>{
    try{
      const config = {
        headers : {
          Authorization: `Bearer ${token}`
        }
      }

      const res = await axios.delete(`http://localhost:5000/teams/${id}`, config)
      getTeams()
    }catch(e){
      console.log("ERROR SA DELETE NG TEAMS")
    }
  }

//pagtawag sa api once meron lang changes sa table
 useEffect(()=>{
  getTeams() //magrarun lang si getTeams pag tinawag si setTeamsData
 }, [setTeamsData])
  

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Teams Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<TeamsForm/>
        </Col>
        <Col md="8" className="">
          <div>
            <Button className="btn-sm border mr-1" onClick={()=>getTeams()}>
              Get All
            </Button>
            <Button className="btn-sm border mr-1" onClick={()=>getTeams("?isActive=true")}>
              Get Active
            </Button>
            <Button className="btn-sm border mr-1" className="btn-sm border mr-1" onClick={()=>getTeams("?isActive=false")}>
              Get Deactive
            </Button>
          </div>
          <hr/>
        	<TeamsTableHead teamsAttr={teams} deleteTeam={deleteTeam} />
        </Col>
      </Row>
    </Container>
  );
}

export default TeamsPage;