import React, { Fragment } from 'react';
import {Link} from 'react-router-dom';
import { Table } from 'reactstrap';

const TaskRow = ({tasksAttr, index, completeTask}) => {
  console.log("TASKS ROW ATTR", tasksAttr)

  const {description, memberId, _id, isCompleted}=tasksAttr;
  let completeBtn = <button className="btn btn-danger ml-1" onClick={()=>completeTask(_id)} ><i class="far fa-trash-alt"></i></button>;
  if(isCompleted) {
    completeBtn=""
  }

  return (

        <tr>
          <th scope="row">{index}</th>
          <td>{description}</td>
          <td>{memberId ? memberId.firstName+" "+memberId.lastName: "N/A"}</td>
          <td>{!isCompleted ? "On-going" : "Completed"}</td>
          <td>
            <Link className="btn btn-primary" to={`/tasks/${_id}`}><i class="far fa-eye"></i></Link>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            {completeBtn}
          </td>
        </tr>

  );
}

export default TaskRow;