import React from 'react';
import { Table } from 'reactstrap';
import MemberTableBody from '../rows/MemberTableBody';

const MemberTableHead = (props) => {
  let row;

    if(!props.membersAttr){ //pwedeng ipasok ang if sa loob ng return by using ternary = !props.teamAttr?statement:result
      row = (
        <tr colSpan="3">
          <em>No Members found..</em>
        </tr>
        )
    }else{
      let i = 0;
      row=(
        props.membersAttr.map( members => {return <MemberTableBody membersAttr={members} key={members._id} index={++i} deleteMember={props.deleteMember} />
        })
      )
    }
  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Team</th>
          <th>Position</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {row}
      </tbody>
    </Table>
  );
}

export default MemberTableHead;